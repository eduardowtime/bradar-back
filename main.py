#!/usr/bin/env python3
#encoding: utf-8

"""
    Script inicial para configuração de inicialização do sistema
"""

__author__= 'Eduardo Bonete de Oliveira'
__date__ = '03/06/2019'
__copyright__ = 'Copyright (c) 2013 Bradar - Embraer Defesa & Segurança'

import os
from flask import Flask, json
from flask_socketio import SocketIO, send
from flask_cors import CORS
from views.calibration import calibration
from views.auth import auth
from views.bit import iobit
from controllers.bit import Bit

app = Flask(__name__, static_url_path='/static')
app.secret_key = os.urandom(12)

socketio = SocketIO(app)
iobit.init_io(socketio)


"""
Separação das rotas por grupo, usando blueprint para definir um prefixo.
"""
app.register_blueprint(calibration, url_prefix='/calibration/')
app.register_blueprint(auth, url_prefix='/auth/')
#app.register_blueprint(bit, url_prefix='/bit/')

"""
Garante acesso via JS
"""
CORS(app)

if __name__ == "__main__":
    import settings
    socketio.run(app, host='0.0.0.0', port=settings.port, debug=settings.debug)