#!/usr/bin/env python3
#encoding: utf-8

"""
Classe responsável pelo tratamento dos dados a serem exibido na sessão de calibração
"""

__author__= 'Eduardo Bonete'
__date__ = '03/06/2019'
__copyright__ = '"Copyright (c) 2013 Bradar - Embraer Defesa & Segurança'

from m200vig.radar_lib.m200vig import M200Vig
from controllers import BitObserver
from flask import jsonify


class Bit:
    def get_forced_bit(self, painel):
        radar = M200Vig()
        return radar.get_forced_bit(painel)

    def set_bit_execution(self, enable):
        observer = BitObserver()
        radar = M200Vig()
        return jsonify(radar.set_bit_execution(enable, observer))