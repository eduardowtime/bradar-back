
#!/usr/bin/env python3
#encoding: utf-8

"""
Classes responsáveis por avaliar o andamento das threads
"""

__author__= 'Eduardo Bonete'
__date__ = '03/06/2019'
__copyright__ = '"Copyright (c) 2013 Bradar - Embraer Defesa & Segurança'


import struct
from typing import List


#Calibration
class CalObserver:

    def __init__(self):
        self.finished = False
        self.error = None
        self.last_pulse_count = None
        self.report = None
        self.unit_id = None
        self.status = None
        self.success = None
        self.iterations = None

    def on_start(self, report, continued):
        self.status = 1

    def on_cancel(self, report):
        self.status = 5
        self.finished()

    def on_progress(self, report, unit_id, status, iterations):
        self.iterations = iterations

    def on_finish(self, report):
        self.report = report.to_formatted_string()
        self.success = True
        self._finished()

    def on_error(self, report, unit_id, error):
        self.report = report.to_formatted_string()
        self.unit_id = unit_id.to_formatted_string()
        self.error = error
        self.success = False
        self._finished()

    def _finished(self):
        self.finished = True


#Raw Data
class ExtractRawDataObserver:

    def __init__(self, path):
        self.finished = False
        self.error = None
        self.last_pulse_count = None
        self._fd = open(path, 'wb')

    def on_incoming_vector(self, vector: List[int], pulses: int,
                           pulses_counter: int) -> None:
        self._fd.write(struct.pack('{}I'.format(len(vector)), *vector))
        self.last_pulse_count = pulses_counter

    def on_error_message(self, error: str, pulses: int,
                         pulses_counter: int) -> None:
        self.error = error
        self._finished(pulses_counter)

    def on_eof(self, pulses: int, pulses_counter: int) -> None:
        self._finished(pulses_counter)

    def on_reach_total_pulses(self, pulses: int, pulses_counter: int) -> None:
        self._finished(pulses_counter)

    def on_involuntary_disconnection(self, pulses: int,
                                     pulses_counter: int) -> None:
        self.error = 'disconnected'
        self._finished(pulses_counter)

    def _finished(self, pulses_counter):
        self._fd.close()
        self.finished = True
        self.last_pulse_count = pulses_counter


#Bit
class BitObserver:
    def __init__(self):
        self.id = None
        self.status = None
        self.response = {}
        self.location = None

    def on_bit_status_change(self, id: int, status: int):
        self.id = id
        self.status = status

    def on_bit_details_update(self, response):
        self.response = response

    def on_bit_summary_update(self, response):
        self.response = response

    def on_bit_timeout(self, location):
        self.location = location