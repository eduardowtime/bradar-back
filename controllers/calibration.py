#!/usr/bin/env python3
#encoding: utf-8

"""
Classe responsável pelo tratamento dos dados a serem exibido na sessão de calibração
"""

__author__= 'Eduardo Bonete'
__date__ = '03/06/2019'
__copyright__ = '"Copyright (c) 2013 Bradar - Embraer Defesa & Segurança'

from m200vig.radar_lib.m200vig import M200Vig
from controllers import CalObserver
from flask import jsonify
import time, json


class Calibration:
    def set_calibration(self, dados):
        radar = M200Vig()
        """
            example of parameters
            {
                "mode": "tx or rx",
                "reference_module": "str",
                "max_retries":int,
                "center_frequency": int
            }
        """
        observer = CalObserver()
        radar.set_calibration(dados, observer)
        count = 0
        while not observer.finished:
            time.sleep(1)

        if observer.success:
            return json.dumps({"success": observer.success,
                               "data": observer.report,
                               "message": "Calibração efetuada com sucesso",
                               "iterations": observer.iterations
                               })
        else:
            return json.dumps({"success": observer.success,
                               "message": observer.error,
                               "iterations": observer.iterations
                               })

    def get_calibration_matrix(self):
        radar = M200Vig()
        return jsonify({"success": True,
                        "data": radar.get_calibration_matrix()})

    def set_calibration_matrix(self, dados):
        radar = M200Vig()