#!/usr/bin/env python3
#encoding: utf-8

"""
Classe responsável pelo tratamento dos dados a serem exibido na sessão de calibração
"""

__author__= 'Eduardo Bonete'
__date__ = '03/06/2019'
__copyright__ = '"Copyright (c) 2013 Bradar - Embraer Defesa & Segurança'

from m200vig.radar_lib.m200vig import M200Vig
from controllers import CalObserver
from flask import jsonify
import time, json


class Authentication:
    def set_login(self, username, password):
        radar = M200Vig()
        try:
            dados = radar.set_login(username, password)
            dados = {'success': True, 'message': '', 'data': dados}
        except Exception as e:
            dados = {'success': False, 'message': 'Dados de acesso inválidos', 'data': {}}
        return jsonify(dados)