#!/usr/bin/env python3
#encoding: utf-8

"""
Rotas responsáveis por apresentar dados TX/RX
Rotas para atualização e dados TX/RX
"""

__author__= 'Eduardo Bonete'
__date__ = '10/06/2019'
__copyright__ = '"Copyright (c) 2013 Bradar - Embraer Defesa & Segurança'

from flask import Blueprint, request, json
from views import exceptions
from controllers.auth import Authentication

auth = Blueprint('auth', __name__)

@auth.route('login', methods=['POST'])
def login():
    if request.method == 'POST':
        try:
            dados = json.loads(request.data.decode('utf-8'))
        except Exception as e:
            return json.dumps({"success": False, "message": "Request data is missing "+str(e.message)}), 500
    else:
        return json.dumps({"success": False, "message": "Method not allowed"}), 405

    auth = Authentication()
    return auth.set_login(dados['username'], dados['password'])