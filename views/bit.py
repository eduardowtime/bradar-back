#!/usr/bin/env python3
#encoding: utf-8

"""
Rotas responsáveis por apresentar dados TX/RX
Rotas para atualização e dados TX/RX
"""

__author__= 'Eduardo Bonete'
__date__ = '10/06/2019'
__copyright__ = '"Copyright (c) 2013 Bradar - Embraer Defesa & Segurança'

from flask import Blueprint, request, json, jsonify
from controllers.bit import Bit
from io_blueprint import IOBlueprint
from flask_socketio import send, emit


#bit = Blueprint('bit', __name__)
iobit = IOBlueprint('/bit')



@iobit.on('connect')
def connect():
    data = [getData(1)]
    emit('message', json.dumps(data))


@iobit.on('message')
def message(painel = 1):
    data = [getData(painel)]
    send(json.dumps(data), broadcast=True)


def get_forced_bit(painel):
    bit = Bit()
    data = bit.get_forced_bit(1)
    return data

def getData(painel):
    bit = Bit()
    data = bit.get_forced_bit(painel)
    return data


"""@bit.route('/', methods=['POST', 'GET'])
def bit_datail():
    try:
        dados = json.loads(request.data.decode('utf-8')) if request.method == 'POST' else request.args.to_dict()
    except Exception as e:
        return json.dumps({"success": False, "message": "Request data is missing "}), 500
    if request.method == 'POST':
        bit = Bit()
        return bit.set_bit_execution(dados['enable'])
    else:
        bit = Bit()
        if 'painel' in dados:
            return bit.get_forced_bit(dados['painel'])
        else:
            return json.dumps({"success": False, "message": "data 'painel' is required "}), 500"""