#!/usr/bin/env python3
#encoding: utf-8

"""
Rotas responsáveis por apresentar dados TX/RX
Rotas para atualização e dados TX/RX
"""

__author__= 'Eduardo Bonete'
__date__ = '27/05/2019'
__copyright__ = '"Copyright (c) 2013 Bradar - Embraer Defesa & Segurança'

from flask import Blueprint, request, json
from controllers.calibration import Calibration

calibration = Blueprint('calibration', __name__)

@calibration.route('set', methods=['POST'])
def set_calibration():
    if request.method == 'POST':
        try:
            dados = json.loads(request.data.decode('utf-8'))
        except Exception as e:
            return json.dumps({"success": False, "message": "Request data is missing "+str(e.message)}), 500
    else:
        return json.dumps({"success": False, "message": "Method not allowed"}), 405
    calibration = Calibration()
    return calibration.set_calibration(dados)


@calibration.route('get_calibration_matrix', methods=['GET'])
def get_calibration_matrix():
    calibration = Calibration()
    return calibration.get_calibration_matrix()

@calibration.route('set_calibration_matrix', methods=['POST'])
def set_calibration_matrix():
    if request.method == 'POST':
        try:
            dados = json.loads(request.data.decode('utf-8'))
        except Exception as e:
            return json.dumps({"success": False, "message": "Request data is missing "+str(e.message)}), 500
    else:
        return json.dumps({"success": False, "message": "Method not allowed"}), 405
    calibration = Calibration()
    return calibration.set_calibration_matrix(dados)