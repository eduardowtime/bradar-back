# -*- coding: utf-8 -*-
"""M200Vig exceptions.

Classes
-------
M200VigOperationException
    exception thrown by operations of the M200Vig
"""


class M200VigOperationException(Exception):
    """Exception thrown by M200Vig operations."""

    pass
