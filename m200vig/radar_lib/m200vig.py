# -*- coding: utf-8 -*-

"""Mock interface of M200Vig library.

This module is a mock interface to used by the WTime team during the development
of CO-HEBE server side. It emulates the comunication with the radar system.

file..: m200vig.py

author: iinfante

date..: Sex Mar 29 08:12:35 -03 2019

"""

import time
import threading
import random
import uuid
import os
import tarfile
import datetime

from typing import List, Dict, Tuple, Union, Set, Callable
from m200vig.radar_lib.exceptions import M200VigOperationException
from m200vig.radar_lib.logs import get_logs, get_locations

_EXPECTED_LOG_SIZE = 1e6

class M200Vig(object):
    """A class used to represent the M200Vig radar."""

    instance = None
    threadLock = threading.Lock()

    class __M200Vig(object):
        """Inner class to ensure that only one instance of M200Vig will be available."""

        def __init__(self):
            # Authentication
            self._auth_dict = {}
            self._auth_profiles = {
                'ADMIN': ['RF::START_TRANSMISSION', 'RF::END_TRANSMISSION', 'RF::GET_RAW_DATA_STATE',
                    'CAL::START_COMPLETE', 'CAL::GETRESULTS_CALIBRATION', 'CAL::APPLYRESULTS_CALIBRATION'],
                'OPERATOR': ['CAL::GETRESULTS_CALIBRATION']
            }
            # BIT
            # self._bit_handler = None
            self._bit_thread = {'1': None, '2': None, '3': None, '4': None}
            # self._bit_execution = False
            self._bit_interval = 10
            # CAL
            self._cal_waiting_time = 20
            self._cal_status = None
            self._cal_last_result = None
            self._cal_thread = None
            # RF
            self._is_antenna_transmitting = {'1': False, '2': False, '3': False, '4': False}
            self._rf_thread = None
            # SCF
            self._scf_matrix = {'1': None, '2': None, '3': None, '4': None}
            self._scf_indexes = {'1': None, '2': None, '3': None, '4': None}
            # MPE
            # self._channels_p1p3 = [None] * 4
            # self._channels_p2p4 = [None] * 4
            self._channels = {'1': [], '2': [], '3': [], '4': []}
            self._blanking_sectors = set()
            # Supervisor
            self._lb_thread = {'1': None, '2': None, '3': None, '4': None}

            # Class members to simulate errors
            self.auth_login_error = False
            self.auth_logout_error = False
            self.auth_login_avc_error = False
            self.auth_validate_token_error = False

            self.bit_set_error = False
            self.bit_get_forced_error = False

            self.cal_get_matrix_error = False
            self.cal_set_matrix_error = False
            self.cal_start_error = False

            self.rf_transmission_get_error = False
            self.rf_transmission_set_error = False
            self.rf_error = False
            self.rf_disconnect = False

            self.scf_set_error = False
            self.scf_get_error = False

            self.mpe_set_channels_error = False
            self.mpe_get_channels_error = False
            self.mpe_get_av_channels_error = False
            self.mpe_set_sectors_error = False
            self.mpe_get_sectors_error = False
            self.mpe_get_av_sectors_error = False

            self.antenna_pos_error = False
            self.antenna_pos_not_rdy = False

            self.lb_test_error = False

            self.reset_error = False
            self.emergency_error = False

    def __init__(self):
        """Constructor - Creates instance only if it not exists."""
        if not M200Vig.instance:
            M200Vig.instance = M200Vig.__M200Vig()
            self._init_parameters()

    def __getattr__(self, name):
        """Override __getattr__ method to get parameters on instance."""
        return getattr(self.instance, name)

    def __setattr__(self, name, args):
        """Override __setattr__ method to modify parameters on instance."""
        return setattr(self.instance, name, args)

    def _init_parameters(self) -> None:
        self._notification = {
            'id': 0,
            'parent': None,
            'name': 'root',
            'desc': 'top level BIT node',
            'type': 'node',
            'points': [],
            'children': [
                {
                    'id': 1,
                    'parent': 0,
                    'name': 'leaf1',
                    'desc': 'BITs from the leaf1 subsystem',
                    'type': 'leaf',
                    'points': [
                        {
                            'id': 1,
                            'name': 'point1',
                            'value': 9,
                            'threshold': 6,
                            'rule': '>='
                        },
                        {
                            'id': 2,
                            'name': 'point2',
                            'value': 0,
                            'threshold': 6,
                            'rule': '<'
                        }],
                    'children': []
                }]
            }

    # Authentication - GAR (AVC)
    def set_login(self, username: str, password: str) -> Dict[str, str]:
        """Tries to login according to given username and password.

        Parameters
        ----------
        username : str
            the user who requested login
        token : str
            the access token

        Raises
        ------
        M200VigOperationException
            If any error occurs and the method could not be executed.

        """
        if self.auth_login_error:
            raise M200VigOperationException
        if username != "".join(reversed(password)):
            raise M200VigOperationException
        access_token = str(uuid.uuid4())
        return self._add_user_info(username, access_token)

    def set_login_avc(self, username: str, token: str) -> Dict[str, str]:
        """Logs on CO-HEBE through AVC.

        Parameters
        ----------
        username : str
            the user who requested login
        token : str
            the access token

        Raises
        ------
        M200VigOperationException
            If any error occurs and the method could not be executed.

        """
        if self.auth_login_avc_error:
            raise M200VigOperationException
        return self._add_user_info(username, token)

    def _add_user_info(self, name: str, token: str) -> Dict[str, str]:
        """Adds user info to authentication dictionary."""
        user_info = {
            'profile': 'ADMIN',
            'username': name,
            'permissions': self._auth_profiles['ADMIN']
        }
        self._auth_dict.update({token: user_info})
        user_info.update({'token': token})
        return user_info

    def set_logout(self, username: str, token: str) -> None:
        """Terminates user session.

        Parameters
        ----------
        username : str
            the user who requested logout
        token : str
            the access token

        Raises
        ------
        M200VigOperationException
            If any error occurs and the method could not be executed.

        """
        if self.auth_logout_error:
            raise M200VigOperationException
        try:
            self._auth_dict.pop(token)
        except KeyError:
            raise M200VigOperationException

    def is_token_valid(self, token: str, service_id: str) -> bool:
        """
        Validates each requisition according to user profile

        :param token: str
        :param service_id: str
        """
        if self.auth_validate_token_error:
            raise M200VigOperationException
        if token in self._auth_dict:
            return self._auth_dict[token]['permissions'].count(service_id) > 0
        return False

    # PCCO-RF
    def get_raw_data(self, panel_id: str, page: str, num_pulse: int, observer: object) -> None:
        """Extract raw data from radar.

        The observer object passed shall be used by the client to collect
        information on the progress of the test. The object must implement
        the following interface:

        * on_incoming_vector(vector: List[int], pulses: int, pulses_counter: int) -> None
        * on_error_message(error: str, pulses: int, pulses_counter: int) -> None
        * on_eof(pulses: int, pulses_counter: int) -> None
        * on_reach_total_pulses(pulses: int, pulses_counter: int) -> None
        * on_involutary_disconnection(pulses: int, pulses_counter: int) -> None

        Parameters
        ----------
        panel_id : str
            the id of the panel
        num_pulse : int
            number of pulses to be extracted
        observer : object
            object that implements a specific interface

        Raises
        ------
        M200VigOperationException
            If any error occurs and the method could not be executed.

        """
        self.threadLock.acquire()
        if self._rf_thread is not None and self._rf_thread.is_alive():
            self._rf_thread.join()

        self._rf_thread = threading.Thread(
            target=self._get_raw_data,
            name='Raw Data Thread',
            args=(num_pulse, observer))
        self._rf_thread.start()
        self.threadLock.release()

    def get_antenna_transmission_state(self, panel_id: str) -> bool:
        """Get state of transmission of the antennas on a given panel.

        Parameters
        ----------
        panel_id : str
            the id of the panel

        Raises
        ------
        M200VigOperationException
            If any error occurs and the method could not be executed.

        """
        if self.rf_transmission_get_error:
            raise M200VigOperationException
        return self._is_antenna_transmitting[panel_id]

    def set_antenna_transmission_state(self, panel_id: str, enable: bool) -> None:
        """Set state of transmission of the antennas on the given panel.

        Parameters
        ----------
        panel_id : str
            the id of the panel

        Raises
        ------
        M200VigOperationException
            If any error occurs and the method could not be executed.

        """
        if self.rf_transmission_set_error:
            raise M200VigOperationException
        if enable:
            self._is_antenna_transmitting[panel_id] = True
        else:
            self._is_antenna_transmitting[panel_id] = False

    # BIT
    def get_forced_bit(self, panel_id: str) -> dict:
        """Get values of last BIT execution on the given panel.

        Parameters
        ----------
        panel_id : str
            the id of the panel

        Raises
        ------
        M200VigOperationException
            If any error occurs and the method could not be executed.

        """
        if self.bit_get_forced_error:
            raise M200VigOperationException
        self._notification['children'][0]['points'][0]['value'] -= 1
        self._notification['children'][0]['points'][1]['value'] += 1
        # if self._bit_handler:
        #     self._bit_handler(self._notification)

        return self._notification

    def set_bit_execution(self, panel_id: str, enable: bool, observer) -> bool:
        """Start or stop monitoring bit verifications on the given panel.

        The observer object passed shall be used by the client to collect
        information on the progress of the test. The object must implement
        the following interface:

        * on_bit_status_change(id: int, status: int)
        * on_bit_details_update(response: dict)
        * on_bit_summary_update(response: dict)
        * on_bit_timeout(location: str)

        Parameters
        ----------
        enable : bool
            flag that indicates monitoring state
        panel_id : str
            the id of the panel
        observer: object
            object that implements a specific interface

        Raises
        ------
        M200VigOperationException
            If any error occurs and the method could not be executed.

        """
        if self.bit_set_error:
            raise M200VigOperationException
        if enable:
            self._start_bit(panel_id, observer)
        else:
            self._disconnect(panel_id)
        return True

    def get_bit_execution(self, panel_id: str) -> bool:
        """Get bit execution status."""
        return self._bit_execution[panel_id].is_alive()

    def get_temporization_values(self) -> int:
        """Return possible temporization values."""
        return list(range(10, 61, 1))

    def get_temporization_value(self) -> int:
        """Set temporization values."""
        return self._bit_interval

    def set_temporization_value(self, value: int) -> None:
        """Set temporization values."""
        self._bit_interval = value
        for thread in self._bit_thread.values():
            if thread is not None and thread.is_alive():
                thread.set_interval(self._bit_interval)

    # CAL
    def set_calibration(self, panel_id: str, cal_parameters: dict, observer: object) -> None:
        """Start calibration on the given panel

        The observer object passed shall be used by the client to collect
        information on the progress of the test. The object must implement
        the following interface:

        * on_start(report: ExecutionReport, continued: bool)
        * on_cancel(report: ExecutionReport)
        * on_pause(report: ExecutionReport)
        * on_finish(report: ExecutionReport)
        * on_progress(report: ExecutionReport, unit_id: CalibrationUnitId, status: int, iterations: int)
        * on_error(report: ExecutionReport, unit_id: CalibrationUnitId, error: str)

        Parameters
        ----------
        panel_id : str
            the id of the panel
        cal_parameters : dict
            dictionary containing the following parameters:
            * mode : str (TX or RX)
            * reference_module : str
            * max_retries : int
            * center_frequency : int
        observer: object
            object that implements a specific interface

        Raises
        ------
        M200VigOperationException
            If any error occurs and the method could not be executed.

        """
        if self.cal_start_error:
            raise M200VigOperationException
        # start
        self._start_cal(cal_parameters, observer)

    def get_calibration_matrix(self) -> List[object]:
        """Return the results of last calibration executed

        Raises
        ------
        M200VigOperationException
            If there is no calibration data to return.

        """
        if self.cal_get_matrix_error:
            raise M200VigOperationException
        # get_results
        if self._cal_last_result is None:
            raise M200VigOperationException
        return self._cal_last_result

    def set_calibration_matrix(self, calibration_unit_results: List[object]) -> None:
        """Applies the given calibration results to a panel

        Parameters
        ----------
        calibration_unit_results : List
            List containing items with the following parameters:
            * calibration_unit_id : CalibrationUnitId
                panel_id : str
                active_element : str
                calibration_type : str
                center_frequency : int
                start_time : long
            * submodule_id : SubmoduleID
                nr_panel : int
                nr_element : int
                nr_submodule : int
            * iterations : int
            * warn_messages : List[str]
            * error_messages : List[str]
            * delta_reference : List[float]
            * snr : List[float]
            * disabled_submodules : List[SubmoduleID]
            * status : CalibrationUnitStatus

        Raises
        ------
        M200VigOperationException
            If any error occurs and the method could not be executed.
        """
        if self.cal_set_matrix_error:
            raise M200VigOperationException
        # apply
        self._cal_last_result = calibration_unit_results

    def get_available_elements(self) -> int:
        """Get number of available elements."""
        return 1

    def get_available_reference_modules(self) -> int:
        """Get number of available reference modules."""
        return 72

    # SCF
    def set_electronic_pointing(self, scan_mode: str, panel_id: str, indexes: List[int],
                                matrix: List[List[Union[int, float]]]) -> None:
        """Set electronic pointing pointers and instructions.

        Parameters
        ----------
        panel_id : str
            the id of the panel
        indexes: List[int]
            a list conainting the
        matrix:
            a matrix of steerings, each line containing the steering
            index, frequency and the ponderations.

        Raises
        ------
        M200VigOperationException
            If any error occurs and the method could not be executed.

        """
        if panel_id not in self._scf_matrix.keys():
            raise M200VigOperationException

        if self.scf_set_error:
            raise M200VigOperationException

        self._scf_indexes[panel_id] = indexes
        self._scf_matrix[panel_id] = matrix

    def get_electronic_pointing(self, panel_id: str) -> Tuple[List[int], List[List[Union[int, float]]]]:
        """Get electronic pointing pointers and instructions.

        Parameters
        ----------
        panel_id : str
            the id of the panel

        Raises
        ------
        M200VigOperationException
            If any error occurs and the method could not be executed.

        """
        if panel_id not in self._scf_matrix.keys():
            raise M200VigOperationException

        if self.scf_get_error:
            raise M200VigOperationException

        matrix = self._scf_matrix[panel_id]
        if matrix is None:
            return list(range(124)), [[j] + [0.0 for i in range(289)] for j in range(124)]
        else:
            return self._scf_indexes[panel_id], self._scf_matrix[panel_id]

    # MPE
    def get_available_channels(self) -> int:
        """Return available channel frequencies.

        Raises
        ------
        M200VigOperationException
            If any error occurs and the method could not be executed.

        """
        if self.mpe_get_av_channels_error:
            raise M200VigOperationException
        # return list(range(1, 11))
        return 10

    # def get_channels(self) -> Tuple[List[int], List[int]]:
    #     """Get channel frequencies.

    #     Raises
    #     ------
    #     M200VigOperationException
    #         If any error occurs and the method could not be executed.

    #     """
    #     if self.mpe_get_channels_error:
    #         raise M200VigOperationException
    #     return self._channels_p1p3, self._channels_p2p4

    # def set_channels(self, channels_p1p3: List[int], channels_p2p4: List[int]) -> None:
    #     """Set channel frequencies.

    #     The same channels will be used for panel 1 and 3, as well as for
    #     panels 2 and 4.

    #     Parameters
    #     ----------
    #     channels_p1p3: List[int]
    #         channels for panels 1 and 3.
    #     channels_p2p4: List[int]
    #         channels for panels 2 and 4.

    #     Raises
    #     ------
    #     M200VigOperationException
    #         If any error occurs and the method could not be executed.

    #     """
    #     if self.mpe_set_channels_error:
    #         raise M200VigOperationException
    #     self._channels_p1p3 = channels_p1p3
    #     self._channels_p2p4 = channels_p2p4

    def get_channels(self, panel_id: str) -> List[int]:
        """Get channel frequency.

        Raises
        ------
        M200VigOperationException
            If any error occurs and the method could not be executed.

        """
        if self.mpe_get_channels_error:
            raise M200VigOperationException
        return self._channels[panel_id]

    def set_channels(self, panel_id: str, channels: List[int]) -> None:
        """Set channel frequency.

        Raises
        ------
        M200VigOperationException
            If any error occurs and the method could not be executed.

        """
        if self.mpe_set_channels_error:
            raise M200VigOperationException
        self._channels[panel_id] = channels

    # Regiões de não-emissão
    def get_radar_sectors(self) -> List[int]:
        """Return available sectors.

        Raises
        ------
        M200VigOperationException
            If any error occurs and the method could not be executed.

        """
        if self.mpe_get_av_sectors_error:
            raise M200VigOperationException
        # return list(range(1, 32))
        return 31

    def get_blanking_sectors(self) -> List[int]:
        """Return currenctly configured blanking sectors.

        Raises
        ------
        M200VigOperationException
            If any error occurs and the method could not be executed.

        """
        if self.mpe_get_sectors_error:
            raise M200VigOperationException
        return self._blanking_sectors

    def set_blanking_sectors(self, sectors: Set[int], enable: bool) -> List[int]:
        """Add or remove blanking sectors.

        Parameters
        ----------
        sectors: Set[int]
            a set of sector to be added or removed from the blanking
            sectors set
        enable: bool
            indicate wether to add or remove blanking sectors

        Raises
        ------
        M200VigOperationException
            If any error occurs and the method could not be executed.

        """
        if self.mpe_set_sectors_error:
            raise M200VigOperationException
        if enable:
            self._blanking_sectors = self._blanking_sectors.union(sectors)
        else:
            self._blanking_sectors = self._blanking_sectors.difference(sectors)

        return self._blanking_sectors

    # PCCO-Supervisor
    def set_loopback_test(self, panel_id: str, observer: object) -> None:
        """Start loopback test.

        The observer object passed shall be used by the client to collect
        information on the progress of the test. The object must implement
        the following interface:

        * on_start(panel: str, test: str, timestamp: float)
        * on_cancel(panel: str, test: str, timestamp: float)
        * on_finish(panel: str, test: str, timestamp: float, report: List[str])
        * on_progress(panel: str, test: str, timestamp: float, msg: str)
        * on_error(panel: str, test: str, timestamp: float, error: str)

        Parameters
        ----------
        panel_id : str
            the id of the panel
        observer: object
            object that implements a specific interface

        """
        self.threadLock.acquire()
        if self._lb_thread[panel_id] is not None and self._lb_thread[panel_id].is_alive():
            self._lb_thread[panel_id].join()

        self._lb_thread[panel_id] = LoopbackTest(panel_id, observer, self.lb_test_error)
        self._lb_thread[panel_id].start()
        self.threadLock.release()

    def cancel_loopback_test(self, panel_id: str) -> None:
        """Cancel loopback test.

        Parameters
        ----------
        panel_id : str
            the id of the panel

        """
        if self._lb_thread[panel_id] is not None and self._lb_thread[panel_id].is_alive():
            self._lb_thread[panel_id].cancel()

    # def set_loopback_normal_mode(self) -> bool:
    #     return True

    def get_antenna_position(self) -> bool:
        """Return wether the antenna is ready for transmission.

        Raises
        ------
        M200VigOperationException
            If any error occurs and the method could not be executed.

        """
        if self.antenna_pos_error:
            raise M200VigOperationException
        if self.antenna_pos_not_rdy:
            return False
        return True

    def global_reset(self) -> None:
        """Send the global reset command.

        Raises
        ------
        M200VigOperationException
            If any error occurs and the method could not be executed.

        """
        if self.reset_error:
            raise M200VigOperationException

    def set_emergency_stop(self) -> None:
        """Send the emergency stop command.

        Raises
        ------
        M200VigOperationException
            If any error occurs and the method could not be executed.

        """
        if self.emergency_error:
            raise M200VigOperationException

    # General
    def get_log_locations(self) -> List[str]:
        """Get log locations.

        Raises
        ------
        M200VigOperationException
            If any error occurs and the method could not be executed.

        """
        return list(get_locations().keys())

    # def get_log_files(self, locations: List[str], start_date: str, end_date: str, contains: str) -> List[str]:
    def get_log_files(self, locations: List[str], date: str, contains: str) -> List[str]:
        """Get log files given a list of locations and filters.

        Parameters
        ----------
        locations: list of locations
        date_range: initial and final date in YYYY-MM-DD format
        contains: a string to be contained in the filenames searched

        Raises
        ------
        M200VigOperationException
            If any error occurs and the method could not be executed.

        """
        files = []
        if contains is None:
            contains = ''

        y, m, d = date.split('-')
        date_string = datetime.date(int(y), int(m), int(d)).strftime('%Y%m%d')
        for location in locations:
            path = get_locations()[location]
            log_files = get_logs()[path]
            files.extend([f for f in log_files if contains in f and date_string in f])

        # if start_date is None:
        #     for location in locations:
        #         path = get_locations()[location]
        #         log_files = get_logs()[path]
        #         files.extend([f for f in log_files if contains in f])
        # else:
        #     if end_date is None:
        #         end = datetime.date.today()
        #     else:
        #         y, m, d = end_date.split('-')
        #         end = datetime.date(int(y), int(m), int(d))
        #     y, m, d = start_date.split('-')
        #     start = datetime.date(int(y), int(m), int(d))

        #     delta = end - start
        #     dates = set()

        #     for i in range(delta.days + 1):
        #         date = start + datetime.timedelta(days=i)
        #         dates.add(date.strftime('%Y-%m-%d'))

        #     for location in locations:
        #         path = get_locations()[location]
        #         log_files = get_logs()[path]
        #         files.extend([f for f in log_files if f[:10] in dates and contains in f])
        return files

    def compress_log_files(self, files_list: List[str], progress: Callable) -> str:
        """Compress selected log files.

        Raises
        ------
        M200VigOperationException
            If any error occurs and the method could not be executed.

        """
        # Mock logs path
        # ts = time.time()
        try:
            os.mkdir('/tmp/mock_logs')
        except FileExistsError:
            pass

        tar = tarfile.open('/tmp/logs.tar.gz', 'w:gz')
        for idx, filename in enumerate(files_list):
            filepath = '/tmp/mock_logs/{}'.format(filename)

            # Mock log
            try:
                with open(filepath, 'x') as fd:
                    for line in self._random_logs(filename):
                        fd.write(line)
            except FileExistsError:
                pass

            tar.add(filepath, arcname=filepath.split('/')[-1])
            progress(idx + 1, len(files_list))
        tar.close()
        # te = time.time()
        # print('Time elapsed: {}'.format(te - ts))
        return '/tmp/logs.tar.gz'

    def _start_bit(self, panel_id: str, observer) -> None:
        """
        Start the BIT service in the radar system. The data will come in an
        asynchronous way and reported to the bit_handler.
        """
        # self.threadLock.acquire()
        # self._bit_handler = observer
        # if self._bit_thread is None or not self._bit_thread.is_alive():
        #     self._bit_thread = threading.Thread(target=self._bit_run, name='BitThread',
        #             args=(self._bit_handler, ))
        #     self._keep_running = True
        #     self._bit_execution = True
        #     self._bit_thread.start()
        # self.threadLock.release()
        self.threadLock.acquire()
        if self._bit_thread[panel_id] is None or not self._bit_thread[panel_id].is_alive():
            self._bit_thread[panel_id] = BitThread(panel_id, observer, self._notification, self._bit_interval)
            self._bit_thread[panel_id].start()
        self.threadLock.release()

    def _disconnect(self, panel_id: str) -> None:
        """
        Stop all the connections with the radar system.
        """
        # self.threadLock.acquire()
        # if self._bit_thread is not None and self._bit_thread.is_alive():
        #     self._keep_running = False
        #     self._bit_execution = False
        #     self._bit_thread.join()
        # self._bit_handler = None
        # self.threadLock.release()
        self.threadLock.acquire()
        if self._bit_thread[panel_id] is not None and self._bit_thread[panel_id].is_alive():
            self._bit_thread[panel_id].cancel()
            self._bit_thread[panel_id].join()
        self.threadLock.release()

    # def _bit_run(self, observer) -> None:
    #     """
    #     Code that runs in the BitThread. It simulates the asynchronous BIT
    #     notifications.

    #     :param observer: The handler to be called when a new notification
    #     from radar system arrives.
    #     """

    #     while self._keep_running:
    #         observer.on_bit_details_update(self._notification)
    #         self._notification['children'][0]['points'][0]['value'] -= 1
    #         self._notification['children'][0]['points'][1]['value'] += 1
    #         time.sleep(self._bit_interval)

    def _start_cal(self, cal_parameters: dict, observer) -> None:
        """
        Start the BIT service in the radar system. The data will come in an
        asynchronous way and reported to the bit_handler.
        """

        self.threadLock.acquire()
        if self._cal_thread is None or not self._cal_thread.is_alive():
            self._cal_thread = threading.Thread(target=self._cal_run, name='CalThread',
                    args=(cal_parameters, observer))
            self._cal_thread.start()
        self.threadLock.release()

    def _cal_run(self, cal_parameters: dict, observer) -> None:
        """
        Code that runs in the BitThread. It simulates the asynchronous BIT
        notifications.
            mode: str (TX or RX)
            reference_module: str
            max_retries: int
            center_frequency: int
        :param observer: The handler to be called when a new notification
        from radar system arrives.

        calibration_unit_id
                panel_id: str
                active_element: str
                calibration_type: str
                center_frequency: int
                start_time: long
            submodule_id
                nr_panel: int
                nr_element: int
                nr_submodule: int
            iterations: int
            warn_messages: List[str]
            error_messages: List[str]
            delta_reference: List[float]
            snr: List[float]
            disabled_submodules: List[SubmoduleID]
            status: str

        """
        list_size = 72

        self._cal_status = 'DOING'
        observer.on_start(Report(), False)
        self._cal_last_result = [{
            'calibration_unit_id': {
                'panel_id': panel_id,
                'active_element': 'element_1_1',
                'calibration_type': cal_parameters['mode'],
                'center_frequency': cal_parameters['center_frequency'],
                'start_time': time.time()
            },
            'submodule_id': {
                'nr_panel': int(panel_id),
                'nr_element': 1,
                'nr_submodule': cal_parameters['reference_module']
            },
            'iterations': cal_parameters['max_retries'],
            'warn_messages': [],
            'error_messages': [],
            'delta_reference': [],
            'snr': [],
            'disabled_submodules': [],
            'status': 'DOING'
        } for panel_id in range(1, 5)]
        for i in range(4):
            observer.on_progress(Report(), CalibrationUnitId(), 3, i + 1)
            time.sleep(self._cal_waiting_time/4)
        self._cal_status = 'DONE'
        for p in range(len(self._cal_last_result)):
            self._cal_last_result[p]['delta_reference'] = [random.random()*360 for i in range(list_size)]
            self._cal_last_result[p]['snr'] = [random.uniform(30,50) for i in range(list_size)]
            self._cal_last_result[p]['status'] = 'DONE'
        observer.on_finish(Report())

    def _get_raw_data(self, num_pulse: int, observer) -> None:
        pulse_counter = 0
        error_on_pulse = int(max(2, min(100, num_pulse))/2)
        num_bytes = 10000
        while True:
            data = [int(random.random()*1e6) for i in range(num_bytes)]
            pulse_counter += 1
            observer.on_incoming_vector(data, num_pulse, pulse_counter)
            # Sim end of pulses
            if pulse_counter == num_pulse:
                observer.on_reach_total_pulses(num_pulse, pulse_counter)
                break
            # Sim EOF
            if pulse_counter == 100:
                observer.on_eof(num_pulse, pulse_counter)
                break
            # Sim error
            if self.rf_error and pulse_counter == error_on_pulse:
                observer.on_error_message(
                    'Error while reading raw data',
                    num_pulse, pulse_counter)
                break
            # Sim disconnection
            if self.rf_disconnect and pulse_counter == error_on_pulse:
                observer.on_involuntary_disconnection(
                    num_pulse, pulse_counter)
                break

    def _random_logs(self, filename):
        logs = []
        i = 0
        while random.random() > 1/_EXPECTED_LOG_SIZE:
            logs.append('{}\tLinha {} do log\n'.format(filename, i))
            i += 1
        return logs

class Report:

    def to_formatted_string(self):
        return \
            "Item 1: Descrição item 1\n\
            Item 2: Descrição item 2\n\
            Item 3: Descrição item 3\n\
            Item 4: Descrição item 4\n\
            Item 5: Descrição item 5\n\
            Item 6: Descrição item 6\n\
            Item 7: Descrição item 7\n\
            Item 8: Descrição item 8\n\
            Item 9: Descrição item 9\n\
            Item 10: Descrição item 10"

class CalibrationUnitId:

    def to_formatted_string(self):
        return \
            "Item 1: Descrição item 1\n\
            Item 2: Descrição item 2\n\
            Item 3: Descrição item 3\n\
            Item 4: Descrição item 4\n\
            Item 5: Descrição item 5"


class LoopbackTest(threading.Thread):
    """Thread that simulates the loopback test."""

    def __init__(self, panel_id: str, observer: object, error: bool = False):
        """Constructor.

        Parameters
        ----------
        panel_id : str
            the id of the panel
        observer : object
            object that implements the interface mentioned in the summary
            of the set_loopback_test method
        error : bool
            wether to trigger error during test execution

        """
        super().__init__(name='Loopback Test Thread')
        self._panel_id = panel_id
        self._observer = observer
        self._error = error
        self._cancel = False

    def cancel(self) -> None:
        """Cancel loopback test."""
        self._cancel = True

    def run(self) -> None:
        """Execute loopback test."""
        finish = 10
        report = ['Log {}'.format(i) for i in range(10)]
        self._observer.on_start(self._panel_id, '', time.time())
        for i in range(finish):
            if self._cancel:
                self._observer.on_cancel(self._panel_id, '', time.time())
                return
            msg = '{}/{}'.format(i + 1, finish)
            self._observer.on_progress(self._panel_id, '', time.time(), msg)
            time.sleep(0.1)

            if self._error and i == int(finish / 2):
                self._observer.on_error(self._panel_id, '', time.time(), 'Error')
                return

        self._observer.on_finish(self._panel_id, '', time.time(), report)


class BitThread(threading.Thread):

    def __init__(self, panel_id: str, observer: object, notification: dict, interval: int, error: bool = False):
        super().__init__(name='Bit {} test thread'.format(panel_id))
        self._panel_id = panel_id
        self._observer = observer
        self._error = error
        self._notification = notification
        self._interval = interval
        self._running = False

    def cancel(self) -> None:
        """Stop bit thread."""
        self._running = False

    def set_interval(self, value) -> None:
        """Set Bit Interval."""
        self._interval = value

    def run(self) -> None:
        """Execute Bit."""
        self._running = True
        while self._running:
            self._observer.on_bit_details_update(self._notification)
            self._notification['children'][0]['points'][0]['value'] -= 1
            self._notification['children'][0]['points'][1]['value'] += 1
            time.sleep(self._interval)
