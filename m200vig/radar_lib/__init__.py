"""M200Vig communication package.

This package is use to communicate with M200Vig radar.

Modules
-------
m200vig
    define a class that represent the M200Vig and it applications.
exceptions
    define exceptions used by the m200vig module.

"""
